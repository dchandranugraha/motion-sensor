package com.example.motionsensor

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Half.EPSILON
import android.util.Log
import android.widget.Button
import android.widget.TextView
import kotlin.math.cos
import kotlin.math.sin
import kotlin.math.sqrt

class MainActivity : AppCompatActivity(), SensorEventListener {

    private lateinit var accelerometerSensor: Sensor
    private lateinit var gyroscopeSensor: Sensor
    private lateinit var sensorManager: SensorManager
    private val accelerationValues = arrayListOf<FloatArray>()
    private val gyroscopeValues = arrayListOf<FloatArray>()
    private val NS2S = 1.0f / 1000000000.0f
    private val deltaRotationVector = FloatArray(4) { 0f }
    private var timestamp: Float = 0f
    private lateinit var tvAcceleration: TextView
    private lateinit var tvGyroscope: TextView
    private lateinit var btnAccelerometer: Button
    private lateinit var btnGyroscope: Button
    private var isAccelerometerStarted = false
    private var isGyroscopeStarted = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViews()
        initSensors()
        initListener()
    }

    private fun initViews() {
        tvAcceleration = findViewById(R.id.tv_acceleration)
        tvGyroscope = findViewById(R.id.tv_gyroscope)
        btnAccelerometer = findViewById(R.id.btn_accelerometer)
        btnGyroscope = findViewById(R.id.btn_gyroscope)
    }

    private fun initSensors() {
        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        accelerometerSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        gyroscopeSensor = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE)
    }

    private fun initListener() {
        btnAccelerometer.apply {
            setOnClickListener {
                handleSensorListener(
                    view = this,
                    sensor = accelerometerSensor,
                    isSensorStarted = isAccelerometerStarted
                ).also {
                    isAccelerometerStarted = !isAccelerometerStarted
                }
            }
        }

        btnGyroscope.apply {
            setOnClickListener {
                handleSensorListener(
                    view = this,
                    sensor = gyroscopeSensor,
                    isSensorStarted = isGyroscopeStarted
                ).also {
                    isGyroscopeStarted = !isGyroscopeStarted
                }
            }
        }
    }

    private fun handleSensorListener(view: Button, sensor: Sensor, isSensorStarted: Boolean) {
        view.apply {
            if (isSensorStarted) {
                text = "Start"
                sensorManager.unregisterListener(this@MainActivity, sensor)
            } else {
                text = "Stop"
                sensorManager.registerListener(
                    this@MainActivity,
                    sensor,
                    SensorManager.SENSOR_DELAY_NORMAL
                )
            }
        }
    }

    override fun onSensorChanged(p0: SensorEvent?) {
        when (p0?.sensor?.type) {
            Sensor.TYPE_GYROSCOPE -> {
                handleGyroscopeSensorEvent(event = p0)
            }

            Sensor.TYPE_ACCELEROMETER -> {
                handleAccelerometerSensorEvent(event = p0)
            }
        }
    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {
        // no-op
    }

    private fun handleGyroscopeSensorEvent(event: SensorEvent?) {
        if (event == null) return
        if (timestamp != 0f && event != null) {
            val dT = (event.timestamp - timestamp) * NS2S
            // Axis of the rotation sample, not normalized yet.
            var axisX: Float = event.values[0]
            var axisY: Float = event.values[1]
            var axisZ: Float = event.values[2]

            // Calculate the angular speed of the sample
            val omegaMagnitude: Float = sqrt(axisX * axisX + axisY * axisY + axisZ * axisZ)

            // Normalize the rotation vector if it's big enough to get the axis
            // (that is, EPSILON should represent your maximum allowable margin of error)
            if (omegaMagnitude > EPSILON) {
                axisX /= omegaMagnitude
                axisY /= omegaMagnitude
                axisZ /= omegaMagnitude
            }

            // Integrate around this axis with the angular speed by the timestep
            // in order to get a delta rotation from this sample over the timestep
            // We will convert this axis-angle representation of the delta rotation
            // into a quaternion before turning it into the rotation matrix.
            val thetaOverTwo: Float = omegaMagnitude * dT / 2.0f
            val sinThetaOverTwo: Float = sin(thetaOverTwo)
            val cosThetaOverTwo: Float = cos(thetaOverTwo)
            deltaRotationVector[0] = sinThetaOverTwo * axisX
            deltaRotationVector[1] = sinThetaOverTwo * axisY
            deltaRotationVector[2] = sinThetaOverTwo * axisZ
            deltaRotationVector[3] = cosThetaOverTwo
        }
        timestamp = event.timestamp.toFloat()
        val deltaRotationMatrix = FloatArray(9) { 0f }
        SensorManager.getRotationMatrixFromVector(deltaRotationMatrix, deltaRotationVector)

        gyroscopeValues.add(deltaRotationMatrix)
        tvGyroscope.text = gyroscopeValues.toDisplayedString()
    }

    private fun handleAccelerometerSensorEvent(event: SensorEvent?) {
        if (event == null) return
        // In this example, alpha is calculated as t / (t + dT),
        // where t is the low-pass filter's time-constant and
        // dT is the event delivery rate.
        val alpha = 0.8f
        val gravity = FloatArray(3) { 0f }
        val linearAcceleration = FloatArray(3) { 0f }

        // Isolate the force of gravity with the low-pass filter.
        gravity[0] = alpha * gravity[0] + (1 - alpha) * event.values[0]
        gravity[1] = alpha * gravity[1] + (1 - alpha) * event.values[1]
        gravity[2] = alpha * gravity[2] + (1 - alpha) * event.values[2]

        // Remove the gravity contribution with the high-pass filter.
        linearAcceleration[0] = event.values[0] - gravity[0]
        linearAcceleration[1] = event.values[1] - gravity[1]
        linearAcceleration[2] = event.values[2] - gravity[2]

        accelerationValues.add(linearAcceleration)
        tvAcceleration.text = accelerationValues.toDisplayedString()
    }

    private fun ArrayList<FloatArray>.toDisplayedString(): String {
        return this.map {
            it.joinToString(prefix = "\n\t[", postfix = "]")
        }.joinToString(prefix = "[",postfix = "\n]")
    }
}